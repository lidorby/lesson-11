#include <iostream>
#include "threads.h"
using namespace std;


void main()
{
	//A

	call_I_Love_Threads();
	
	//B+C
	callGetPrimes(0, 1000);
	callGetPrimes(0, 100000);
	//callGetPrimes(0, 1000000);
	
	//D+E
	callWritePrimesMultipleThreads(0,1000,"C:\\Users\\magshimim\\Desktop\\magshimim\\year 2\\cpp\\lesson 11\\me1.txt",5);
	callWritePrimesMultipleThreads(0, 100000, "C:\\Users\\magshimim\\Desktop\\magshimim\\year 2\\cpp\\lesson 11\\me2.txt", 5);
	callWritePrimesMultipleThreads(0, 1000000, "C:\\Users\\magshimim\\Desktop\\magshimim\\year 2\\cpp\\lesson 11\\me3.txt", 5);

	system("pause");
}
